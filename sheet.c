/********************************************************************************************************************************************************************************;
* Project           : Sheet Editor
        *
        * Program name      : sheet.c
        *
        * Author            : Viktor Kostka -xkost16
        *
        * Date created      : 20201028
*
* Purpose    : The aim of the project is to create a program that will implement the basic operations of spreadsheets.
*              The input of the program will be text data, the input of operations will be through arguments of the command line and the program will output result to the terminal.
*
* PS: So far, editing commands are implemented in the program at this stage.
*
|**********************************************************************************************************************************************************************************/

#include <stdio.h>
#include <stdlib.h>
//#include <errno.h>
#include <string.h>
#include "stdbool.h"


#define PATTERN ("Please use correct form! : ./Name_of_program [-d delimer] [name_of_function + [values[][]] <input_file >output_file")
#define FILLE_ERR "The specified file does not meet all the conditions"

#define MAX_LINE 10242
#define MAX_CELL 101

//Structs to save parameters of entered comands
typedef void (*pointer)();
typedef struct EditTab{
    char *cmd;
    long str;
    long end;
    pointer function;
}edittab;

typedef struct SelectRow{
    char *cmd;
    long str_row;
    long end_row;
    char *string;
    pointer function;
}selector;

typedef struct DataProcesing{
    char *cmd;
    char *string;
    long c;
    long n;
    long m;
    pointer function;
}dataproces;

//Devide cell by delimiter
void line_delimer(char *line, char *delimers)
{
    size_t line_length = strlen(line);
    for (int i = 0; i < line_length; i++)
    {
        if (strchr(delimers, line[i]) != NULL)
            line[i] = delimers[0];
    }
}
// Error printer
char usage(char *issue) {
    fprintf(stderr, "ERROR : %s \n", issue);
    return 0;
}

// The function returns the start and end indexes of the columns
int GetColIndex(char *line, int start, int end, char *delimiter){
    //TODO
    int index = 0;
    return index;
}

// Delimiter search
bool find_delimiter(int count, char **argv) {
    if (count < 2) {    //minimum of arguments
        usage(PATTERN);
    }
    if (strcmp(argv[1] , "-d" ) == 0){
        return true;
    }
    else{
        return false;
    }
}
//The function returns valid ints to save to struct
long validInt(char *findint){
    char *string = {0};
    unsigned long result;
    result = strtol(findint, &string, 10);
    if (strlen(string) != 0 || strcmp(string, "-") != 0 )
        return result;
    else if (strcmp(string, "-") == 0)
        return -1;
    else if (strcmp(string, "-") != 0 || strcmp(string, NULL ) != 0)
        usage("Unexpected input!");
    else{
        usage(PATTERN);
        return -1;
    }
}
//Edit functios
//******************************************************************************//

void irow (char *line, unsigned long count, char *delimiter, int delim_count, edittab editor){
    int i;
    if (count == editor.str ) {
        for (i = 0; i < strlen(line); i++) {
            if (strchr(delimiter, line[i]) != NULL) {
                printf("%c", delimiter[0]);
            }
        }
        printf("\n");
    }

}

void arow (char *line, unsigned long count, char *delimiter, int delim_count, edittab editor){
    int i;
    int c = getchar();
    if (c == EOF) {
        for (i = 0; i < delim_count; i++) {
                printf("%c", delimiter[0]);
            }
        printf("\n");
    }
    ungetc(c, stdin);

}

void drow (char *line, unsigned long count, char *delimiter, int delim_count, edittab editor) {
    if(count == editor.str){
        memset(line, 0, sizeof( line));
    }

}


void drows (char *line, unsigned long count, char *delimiter, int delim_count, edittab editor) {
    if(editor.str+1 >= count && count <= editor.end-1 ) {
        memset(line, 0, sizeof(line));
    }

}


void icol (char *line, unsigned long count, char *delimiter, int delim_count, edittab editor) {
    int delims = 0;
    for(int i =0; i < strlen(line); i++ ) {
        if (strchr(delimiter, line[i]) != NULL) {
            delims++;
            if (delims == editor.str) {
                line[i] = delimiter[0];
            }
        }
    }
}


void acol (char *line, unsigned long count, char *delimiter, int delim_count, edittab editor) {
    size_t position = strlen(line);
    printf("%d\n", position);
    line[position-1] = delimiter[0];

}


void dcol (char *line, unsigned long count, char *delimiter, int delim_count, edittab editor) {
    int delims = 0;
    int j = 0;
    for(int i =0; i < strlen(line); i++ ) {
        if (strchr(delimiter, line[i]) != NULL) {
            delims++;
            while (editor.str+1 >= delims && delims <= editor.end-1){
                j++;
                memmove(&line[i], &line[i+j], strlen(&line[i+j])+1);

            }

        }
    }
}


void dcols (char *line, unsigned long count, char *delimiter, int delim_count, edittab editor) {
    int delims = 0;
    int j = 0;
    for(int i =0; i < strlen(line); i++ ) {
        if (strchr(delimiter, line[i]) != NULL) {
            delims++;
            while (delims == editor.str && delims == editor.str+1){
                j++;
                memmove(&line[i], &line[i+j], strlen(&line[i+j])+1);

            }

        }
    }
}



//Data procesing functions
//******************************************************************************//

void cset (char *line, unsigned long count, char *delimiter, int delim_count, edittab editor) {
    printf("I apologize for the inconvenience, the feature will be implemented soon, maybe.\n");

}
void tolower_ (char *line, unsigned long count, char *delimiter, int delim_count, edittab editor) {
    printf("I apologize for the inconvenience, the feature will be implemented soon, maybe.\n");
}
void toupper_ (char *line, unsigned long count, char *delimiter, int delim_count, edittab editor) {
    printf("I apologize for the inconvenience, the feature will be implemented soon, maybe.\n");
}
void round_ (char *line, unsigned long count, char *delimiter, int delim_count, edittab editor) {
    printf("I apologize for the inconvenience, the feature will be implemented soon, maybe.\n");
}
void int_ (char *line, unsigned long count, char *delimiter, int delim_count, edittab editor) {
    printf("I apologize for the inconvenience, the feature will be implemented soon, maybe.\n");
}
void copy (char *line, unsigned long count, char *delimiter, int delim_count, edittab editor){
    printf("I apologize for the inconvenience, the feature will be implemented soon, maybe.\n");
}
void swap (char *line, unsigned long count, char *delimiter, int delim_count, edittab editor) {
    printf("I apologize for the inconvenience, the feature will be implemented soon, maybe.\n");
}
void move (char *line, unsigned long count, char *delimiter, int delim_count, edittab editor) {
    printf("I apologize for the inconvenience, the feature will be implemented soon, maybe.\n");
}
// Find and save selection functions to struct

//******************************************************************************//
bool FindSelector(int count, char **arguments, selector *select_row) {
    int i = 0;
    while (count > i) {
        i++;
        if (strcmp(arguments[i], "rows") == 0) {
            select_row->cmd = arguments[i];
            long str_row_start = validInt(arguments[i + 1]);
            long str_row_end = validInt(arguments[i + 2]);
            if (str_row_start > -1 && str_row_end > -1) {
                select_row->str_row = str_row_start;
                select_row->end_row = str_row_end;
            } else {
                usage(PATTERN);
                return false;
            }
            return true;
        } else if (strcmp(arguments[i], "beginswith") == 0) {
            select_row->cmd = arguments[i];
            select_row->string = arguments[i + 1];
            long str_row_start = validInt(arguments[i + 1]);
            if (str_row_start > -1) {
                select_row->str_row = str_row_start;
            } else {
                usage(PATTERN);
                return false;
            }
            return true;
        } else if (strcmp(arguments[i], "contains") == 0) {
            select_row->cmd = arguments[i];
            select_row->string = arguments[i + 2];
            long str_row_start = validInt(arguments[i + 1]);
            if (str_row_start > -1) {
                select_row->str_row = str_row_start;
            } else {
                usage(PATTERN);
                return false;
            }
            return true;
        }
    }
    return false;
}
// Find and save edit functions to struct
//******************************************************************************//

bool FindEdit(int count, char **arguments, edittab *editor){
    int i = 0;
    while(count >i){
        i++;
        if (strcmp(arguments[i], "irow")==0) {
            editor->cmd = arguments[i];
            editor->function=irow;
            long edit_start = validInt(arguments[i + 1]);
            if (edit_start > -1) {
                editor->str = edit_start;
            }
            else{
                usage(PATTERN);
                return false;
            }
            return true;
        }
        else if(strcmp(arguments[i], "arow")==0){
            editor->function=arow;
            editor -> cmd = arguments[i];
            return true;
        }

        else if(strcmp(arguments[i], "drow")==0) {
            editor->function=drow;
            editor-> cmd = arguments[i];
            long edit_start = validInt(arguments[i + 1]);
            if (edit_start > -1) {
                editor->str = edit_start;
            }
            else{
                usage(PATTERN);
                return false;
            }
            return true;
        }
        else if(strcmp(arguments[i], "drows")==0) {
            editor->function=drows;
            editor-> cmd = arguments[i];
            long edit_start = validInt(arguments[i + 1]);
            long edit_end= validInt(arguments[i + 2]);
            if (edit_start > -1 && edit_end > -1) {
                editor->str = edit_start;
                editor->end = edit_end;
            }
            else{
                usage(PATTERN);
                return false;
            }
            return true;
        }
        else if(strcmp(arguments[i], "icol")==0) {
            editor->function=icol;
            editor-> cmd = arguments[i];
            long edit_start = validInt(arguments[i + 1]);
            if (edit_start > -1) {
                editor->str = edit_start;
            }
            else{
                usage(PATTERN);
                return false;
            }
            return true;
        }

        else if(strcmp(arguments[i], "acol")==0) {
            editor->function=acol;
            editor-> cmd = arguments[i];
            return true;
        }

        else if(strcmp(arguments[i], "dcol")==0) {
            editor->function=dcol;
            editor-> cmd = arguments[i];
            long edit_start = validInt(arguments[i + 1]);
            if (edit_start > -1) {
                editor->str = edit_start;
            }
            else{
                usage(PATTERN);
                return false;
            }
            return true;
        }

        else if(strcmp(arguments[i], "dcols")==0) {
            editor->function=dcols;
            editor-> cmd = arguments[i];
            long edit_start = validInt(arguments[i + 1]);
            long edit_end= validInt(arguments[i + 2]);
            if (edit_start > -1 && edit_end > -1) {
                editor->str = edit_start;
                editor->end = edit_end;
            }
            else{
                usage(PATTERN);
                return false;
            }
            return true;
        }

    }
    return false;
}
// Find and save Data procesing functions to struct
//******************************************************************************//

bool FindDataProces(int count, char **arguments, dataproces *proces) {
    int i = 0;
    while (count > i) {
        i++;
        if (strcmp(arguments[i], "cset") == 0) {
            proces->function = cset;
            proces->cmd = arguments[i];
            proces->string = arguments[i + 1];
            long c = validInt(arguments[i + 2]);
            if (c > -1) {
                proces->c = c;
            } else {
                usage(PATTERN);
                return false;
            }
            return true;
        }
        if (strcmp(arguments[i], "tolower") == 0) {
            proces->function = tolower_;
            proces->cmd = arguments[i];
            long c = validInt(arguments[i + 1]);
            if (c > -1) {
                proces->c = c;
            } else {
                usage(PATTERN);
                return false;
            }
            return true;
        }
        if (strcmp(arguments[i], "toupper") == 0) {
            proces->function = toupper_;
            proces->cmd = arguments[i];
            long c = validInt(arguments[i + 1]);
            if (c > -1) {
                proces->c = c;
            } else {
                usage(PATTERN);
                return false;
            }
            return true;
        }
        if (strcmp(arguments[i], "round") == 0) {
            proces->function = round_;
            proces->cmd = arguments[i];
            long c = validInt(arguments[i + 1]);
            if (c > -1) {
                proces->c = c;
            } else {
                usage(PATTERN);
                return false;
            }
            return true;
        }
        if (strcmp(arguments[i], "int") == 0) {
            proces->function = int_;
            proces->cmd = arguments[i];
            long c = validInt(arguments[i + 1]);
            if (c > -1) {
                proces->c = c;
            } else {
                usage(PATTERN);
                return false;
            }
            return true;
        }
        if (strcmp(arguments[i], "copy") == 0) {
            proces->function = copy;
            proces->cmd = arguments[i];
            long n = validInt(arguments[i + 1]);
            long m = validInt(arguments[i + 2]);
            if (n > -1 && m > -1) {
                proces->c = n;
                proces->c = m;
            } else {
                usage(PATTERN);
                return false;
            }
            return true;
        }
        if (strcmp(arguments[i], "swap") == 0) {
            proces->function = swap;
            proces->cmd = arguments[i];
            long n = validInt(arguments[i + 1]);
            long m = validInt(arguments[i + 2]);
            if (n > -1 && m > -1) {
                proces->c = n;
                proces->c = m;
            } else {
                usage(PATTERN);
                return false;
            }
            return true;
        }
        if (strcmp(arguments[i], "move") == 0) {
            proces->function = move;
            proces->cmd = arguments[i];
            long n = validInt(arguments[i + 1]);
            long m = validInt(arguments[i + 2]);
            if (n > -1 && m > -1) {
                proces->c = n;
                proces->c = m;
            } else {
                usage(PATTERN);
                return false;
            }
            return true;
        }
    }
    return false;
}

void LinePrint(char *line)
{
    size_t line_length = strlen(line);
    if (line_length == 1 && line[0] == '\n')
        return;
    printf("%s", line);
}
//MAIN
//******************************************************************************//

int main(int argc, char *argv[]) {

    //DELIMITER
    int size_of_deli = 2;
    bool find_deli = find_delimiter(argc, argv);
    if (find_deli)
        size_of_deli = strlen(argv[2]);
    char delimiter[size_of_deli];
    if (find_deli)
        strcpy(delimiter, argv[2]);
    else
        strcpy(delimiter, " ");

    //Line procesing
    char line[MAX_LINE];
    unsigned long long line_counter = 0;
    while (fgets(line, MAX_LINE, stdin)) {
        line_delimer(line, delimiter);
        line_counter++;
        int delim_count = 0;
        if (line_counter == 1) {
            for (int i = 0; i < strlen(line); i++) {
                if (strchr(delimiter, line[i]) != NULL) {
                    delim_count++;
                }
            }
        }

        // selector select_row;
        //if (FindSelector(argc, argv, &select_row))
            // selector.function(&line, line_counter,delimiter, delim_count, selector);

        edittab editor;
       if (FindEdit(argc, argv, &editor))
           editor.function(line, line_counter,delimiter, delim_count, editor);

/*
        dataproces proces;
        if (FindDataProces(argc, argv, &proces))
            proces.function(&line, line_counter,delimiter, delim_count, proces);
*/
        LinePrint(line);
       }

        return 0;

}
